<!DOCTYPE html>
<script type="text/javascript" src="topbar.js"></script>
<script type="text/javascript" src="createuser.js"></script>

<link rel="stylesheet" href="createuser.css">
<link rel="stylesheet" href="topbar.css">
<link rel="stylesheet" href="board.css">

<body>
<?php
require 'create-account-controller.php';

if (isset($_POST['submitb'])) {
    $cac = new CreateAccController($_POST['name'], $_POST['usrnm'], $_POST['pswd'], $_POST['number'], $_POST['profilename']);
    $vd = $cac->validateDetails();

    if($vd == "emptyInput") {
        echo '<script> alert("Empty Input!"); </script>';
    }

    else if($vd == "UExist") {
        echo '<script> alert("Username already exist. Please enter again."); </script>';
    }
    
    else if($vd == "PExist") {
        echo '<script> alert("Phone already exist. Please enter again."); </script>';
    }
    
    else if($vd == "UPExist") {
       echo '<script> displayFail(); </script>';
    }
    
    else {
        $cac->addUser();
        echo '<script> displaySuccess(); </script>';
    }
}
?>

<!-- for the heading of the website -->
    <div class="header">
        <a href="../Home/home.html">
            Company Name
        </a>
        <span class="username">
            Welcome, Username
<!-- to allow user to click on the user icon to logout -->
            <div class="dropdown">
                <button class="dropbtn"><img src="../pictures/apple.png"></button>
                <div class="dropdown-content">
                  <button>User Profile</button>
                  <button>Settings</button>
                  <button onclick="displayAlert();">Logout</button>
                </div>
              </div>
        </span> 
    </div>

<!-- allow users to move around to different pages related to user accounts -->
    <div class="board">
        <span>User Accounts</span><br><br>
        <a href="../html/createuser.html" class="profile" id="add">
            Add User
        </a>
        <a href="../html/viewuser.html" class="profile">
            View User
        </a>
        <a href="../html/suspenduser.html" class="profile">
            Suspend User
        </a>
        <a href="../html/updateuser.html" class="profile">
            Update User
        </a>
        <br>
    </div>

<!-- users will key in new user account in these inputs -->
<!-- form action will be added once php is done -->
    <form method="post">
    <div class="profile">
        <span class="options">
            <label for="username">Username:</label>
            <input type="text" id="username" name="usrnm">
            <br><br>

            <label for="password">Password:</label>
            <input type="password" id="password" onfocusout="validatePassword()" name="pswd">
            <span id="passwordError"></span>
            <br><br>

            <label for="repassword">Re-type Password:</label>
            <input type="password" id="repassword" onfocusout="validateRePassword()">
            <span id="repasswordError"></span>
            <br><br>

            <label for="name">Account Name:</label>
            <input type="text" id="name" name="name">
            <br><br>
        </span>

<!-- users will key in new user account in these inputs as well-->
    <div id="describe">
        <label for="number">Phone Number:</label>
        <input type="text" id="number" name="number" onfocusout="validateNumber()">
        <span id="numberError"></span>
          <br><br>

<!-- options showed will depend on database -->
          <label for="profilename" id="profileimg">User Profile:</label>
          <select name="profilename" id="profilename">
            <option value="User Admin">User Admin</option>
            <option value="Manager">Restaurant Manager</option>
            <option value="Staff">Restaurant Staff</option>
            <option value="Owner">Restaurant Owner</option>
          </select>
          <br><br><br>
<!-- user will click on this button and it will submit the new info to database -->
        <span>
            <button id="submit" name="submitb">
               Create Account
            </button>
        </span>
    </div>
     <br>

    </div>
    </form>
</body> 