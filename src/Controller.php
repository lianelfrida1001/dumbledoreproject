<?php



class Controller
{
    public $full_name;
    public $username;
    public $password;
    public $phone_number;
    public $profile;
    public $status;   
    public $description;
    public $functions;
    public $userprofile;
    
    public $name;

    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getfull_name()
    {
        return $this->full_name;
    }
    public function setfull_name($full_name)
    {
        $this->full_name = $full_name;
    }
    public function getPassword()
    {
        return $this->password;
    }
    public function setPassword($password)
    {
        $this->password = $password;
    }
    public function getusername()
    {
        return $this->username;
    }
    public function setusername($username)
    {
        $this->username = $username;
    }
    public function getphonenumber()
    {
        return $this->phone_number;
    }
    public function setphonenumber($phone_number)
    {
        $this->phone_number = $phone_number;
    }   
    public function getprofile()
    {
        return $this->profile;
    }
    public function setprofile($profile)
    {
        $this->profile = $profile;
    }
    public function getstatus()
    {
        return $this->status;
    }
    public function setstatus($status)
    {
        $this->status = $status;
    }
    public function getFormDetails(){

        return $this->full_name.$this->username.$this->password.$this->phone_number.$this->profile.$this->status;
    }
    public function getLoginDetails()
    {
        return $this->username.$this->password.$this->profile;
    }
    public function getInvalidFormDetails()
    {
        return $this->full_name.$this->username.$this->password.$this->phone_number.$this->profile;
    }   
    public function getInvalidLoginDetails()
    {
        return $this->username.$this->password.$this->phone_number;
    }
    public function getInvalid()
    {
        return $this->username;
    }
    public function getUserProfile()
    {
        return $this->up;
    }
    public function setUserProfile($up)
    {
        $this->up = $up;
    }
    public function getDesc()
    {
        return $this->desc;
    }
    public function setDesc($desc)
    {
        $this->desc= $desc;
    }
    public function getFunc()
    {
        return $this ->func;
    }
    public function setFunc($func)
    {
        $this->func = $func;
    }
}
